<?php
/**
 * @version    2.9.x
 * @package    K2
 * @author     JoomlaWorks https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2019 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php if($params->get('itemPreText')): ?>
    <?php echo $params->get('itemPreText'); ?>
    <?php endif; ?>


    <?php 
        $startDate = strtotime("2018-12-31");
        $today = time();
        $day = round(($today - $startDate) / (60*60*24));
        $itemNo = count($items);
        $index = $day%$itemNo;
        $item = $items[$index];
     ?>

    <div class="tip visible-phone center">
        <?php if($params->get('itemIntroText')): ?>
        <?php echo $item->introtext; ?>
        <?php endif; ?>
    </div>
    <div class="hidden-phone">
      <img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
    </div>

</div>
