<?php
/**
 * @version    2.9.x
 * @package    K2
 * @author     JoomlaWorks https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2019 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php if(isset($items) && count($items)): ?>
    <div>
        <?php foreach ($items as $key=>$item):  ?>
        <div class="qa-item g-grid">

        <div class="g-block size-45">
            <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
                <div class="moduleItemIntrotext">
                    <?php if($params->get('itemImage') && isset($item->image)): ?>
                    <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
                        <img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
                    </a>
                    <?php endif; ?>

                    <?php if($params->get('itemIntroText')): ?>
                    <?php echo $item->introtext; ?>
                    <?php endif; ?>
                </div>
                <?php endif; ?>
        </div>

        <div class="g-block size-55">
        <?php if($params->get('itemTitle')): ?>
            <h4><a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h4>
            <?php endif; ?>
            <div class="clr"></div>
            <div class="modItemCategory">
                <?php if($params->get('itemCategory')): ?>
                <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
                <?php endif; ?>
            </div>

            <div class="clr"></div>
            <?php if($params->get('itemCustomLink')): ?>
            <a class="moduleCustomLink" href="<?php echo $itemCustomLinkURL; ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>">
                <?php echo $itemCustomLinkTitle; ?>
            </a>
            <?php endif; ?>
        </div>




            <?php if($params->get('itemReadMore') && $item->fulltext): ?>
            <a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
                <?php echo JText::_('K2_READ_MORE'); ?>
            </a>
            <?php endif; ?>

            <!-- Plugins: AfterDisplay -->
            <?php echo $item->event->AfterDisplay; ?>

            <!-- K2 Plugins: K2AfterDisplay -->
            <?php echo $item->event->K2AfterDisplay; ?>

            <div class="clr"></div>
        </div>
        <?php endforeach; ?>
    </div>
    <?php endif; ?>
</div>
