<?php
/**
 * @version    2.9.x
 * @package    K2
 * @author     JoomlaWorks https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2019 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php $item = $items[0]; ?>

    <div class="g-grid">
        <div class="g-block size-3 modTitle verticalTitle center hidden-phone">
            <?php echo JText::_('ARTICLE_OF_THE_WEEK'); ?>
        </div>
        <div class="g-block size-3 modTitle visible-phone center">
            <?php echo JText::_('ARTICLE_OF_THE_WEEK'); ?>
        </div>
        <div class="g-block size-33">
            <?php if($params->get('itemImage')): ?>
            <div class="moduleItemIntrotext">
                <?php if($params->get('itemImage') && isset($item->image)): ?>
                <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
                    <img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
                </a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
        </div>
        <div class="g-block size-64 intro">
            <h3 class="">
                <?php if($params->get('itemTitle')): ?>
                <a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a>
                <?php endif; ?>
            </h3>
            <div class="clr"></div>
            <?php if($params->get('itemIntroText')): ?>
            <div class="moduleItemIntrotext">
                <?php if($params->get('itemIntroText')): ?>
                <?php echo $item->introtext; ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <div class="clr"></div>
            <?php if($params->get('itemAuthor')): ?>
            <div class="moduleItemAuthor">
                <?php if($params->get('itemAuthorAvatar')): ?>
                <a class="k2Avatar moduleItemAuthorAvatar" rel="author" href="<?php echo $item->authorLink; ?>">
                    <img src="<?php echo $item->authorAvatar; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" style="width:<?php echo $avatarWidth; ?>px;height:auto;" />
                </a>
                <?php endif; ?>

                <?php if(isset($item->authorLink)): ?>
                <a rel="author" title="<?php echo K2HelperUtilities::cleanHtml($item->author); ?>" href="<?php echo $item->authorLink; ?>">
                    <?php echo $item->author; ?>
                </a>
                <?php else: ?>
                <?php echo $item->author; ?>
                <?php endif; ?>
                //
                <?php if($params->get('itemCategory')): ?>
                <a class="moduleItemCategory" href="<?php echo $item->categoryLink; ?>"><?php echo $item->categoryname; ?></a>
                <?php endif; ?>
            </div>
            <?php endif; ?>
            <div class="clr"></div>

            <div class="pull-right">
            <?php if($params->get('itemReadMore') && $item->fulltext): ?>
            <a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
                <?php echo JText::_('K2_READ_MORE'); ?>
            </a>
            <?php endif; ?>
            </div>
  
        </div>
    </div>
</div>
