<?php
/**
 * @version    2.9.x
 * @package    K2
 * @author     JoomlaWorks https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2019 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php if($params->get('itemPreText')): ?>
    <p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
    <?php endif; ?>

    <?php if(isset($items) && count($items)): ?>
    <ul>
        <?php foreach ($items as $key=>$item):  ?>
        <li class="">
           

            <?php if($params->get('itemTitle')): ?>
            <h4><a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h4>
            <?php endif; ?>

            <?php
                $from = $item->extraFields->apo->value;
                $to = $item->extraFields->mechri->value;
            ?>
           
            <?php if($params->get('itemExtraFields') && isset($item->extra_fields) && count($item->extra_fields)): ?>
            <div class="moduleItemExtraFields">
                <ul>
                    <?php foreach ($item->extra_fields as $key => $extraField): ?>
                    <?php if($extraField->id == 9 || $extraField->id == 10  || $extraField->id == 11): ?>
                    <li class="">
                        <?php if($extraField->id == 9 || $extraField->id == 10): ?>

                            <?php if($extraField->id == 9 AND $from === $to): ?>
                                <?php echo ''; ?>
                            <?php elseif($extraField->id == 10 AND $from == $to):?>
                            <span class="moduleItemExtraFieldsLabel pull-left"><?php echo JText::_('K2_DAILY_EVENT'); ?></span>
                            <span class="moduleItemExtraFieldsValue pull-right"><?php echo $extraField->value; ?></span>
                            <div class="clr"></div>
                            <?php else:?>
                            <span class="moduleItemExtraFieldsLabel"><?php echo $extraField->name; ?></span>
                            <span class="moduleItemExtraFieldsValue"><?php echo $extraField->value; ?></span>
                            <?php endif;?>
                        <?php elseif ($extraField->id == 11): ?>
                        <button class="calendarButton"><?php echo $extraField->value; ?></button>
                        <?php endif; ?>
                    </li>
                    <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
            </div>
            <?php endif; ?>


        </li>
        <hr>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>

</div>
