<?php
/**
 * @version    2.9.x
 * @package    K2
 * @author     JoomlaWorks https://www.joomlaworks.net
 * @copyright  Copyright (c) 2006 - 2019 JoomlaWorks Ltd. All rights reserved.
 * @license    GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
 */

// no direct access
defined('_JEXEC') or die;

?>

<div id="k2ModuleBox<?php echo $module->id; ?>" class="k2ItemsBlock<?php if($params->get('moduleclass_sfx')) echo ' '.$params->get('moduleclass_sfx'); ?>">

    <?php if($params->get('itemPreText')): ?>
    <p class="modulePretext"><?php echo $params->get('itemPreText'); ?></p>
    <?php endif; ?>

    <?php if(isset($items) && count($items)): ?>
    <ul>
        <?php foreach ($items as $key=>$item):  ?>
        <li class="event">

            <?php if($params->get('itemImage') || $params->get('itemIntroText')): ?>
            <div class="moduleItemIntrotext">
                <?php if($params->get('itemImage') && isset($item->image)): ?>
                <a class="moduleItemImage" href="<?php echo $item->link; ?>" title="<?php echo JText::_('K2_CONTINUE_READING'); ?> &quot;<?php echo K2HelperUtilities::cleanHtml($item->title); ?>&quot;">
                    <img src="<?php echo $item->image; ?>" alt="<?php echo K2HelperUtilities::cleanHtml($item->title); ?>" />
                </a>
                <?php endif; ?>

                <?php if($params->get('itemIntroText')): ?>
                <?php echo $item->introtext; ?>
                <?php endif; ?>
            </div>
            <?php endif; ?>

            <div class="clr"></div>

            <?php if($params->get('itemTitle')): ?>
            <h4><a class="moduleItemTitle" href="<?php echo $item->link; ?>"><?php echo $item->title; ?></a></h4>
            <?php endif; ?>

            <?php if($params->get('itemTags') && isset($item->tags) && count($item->tags) > 0): ?>
            <div class="moduleItemTags">
                <b><?php echo JText::_('K2_TAGS'); ?>:</b>
                <?php foreach ($item->tags as $tag): ?>
                <a href="<?php echo $tag->link; ?>"><?php echo $tag->name; ?></a>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

            <?php if($params->get('itemAttachments') && isset($item->attachments) && count($item->attachments)): ?>
            <div class="moduleAttachments">
                <?php foreach ($item->attachments as $attachment): ?>
                <a title="<?php echo K2HelperUtilities::cleanHtml($attachment->titleAttribute); ?>" href="<?php echo $attachment->link; ?>">
                    <?php echo $attachment->title; ?>
                </a>
                <?php endforeach; ?>
            </div>
            <?php endif; ?>

            <?php if($params->get('itemCommentsCounter') && $componentParams->get('comments')): ?>
            <?php if(!empty($item->event->K2CommentsCounter)): ?>
            <!-- K2 Plugins: K2CommentsCounter -->
            <?php echo $item->event->K2CommentsCounter; ?>
            <?php else: ?>
            <?php if($item->numOfComments>0): ?>
            <a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
                <?php echo $item->numOfComments; ?> <?php if($item->numOfComments>1) echo JText::_('K2_COMMENTS'); else echo JText::_('K2_COMMENT'); ?>
            </a>
            <?php else: ?>
            <a class="moduleItemComments" href="<?php echo $item->link.'#itemCommentsAnchor'; ?>">
                <?php echo JText::_('K2_BE_THE_FIRST_TO_COMMENT'); ?>
            </a>
            <?php endif; ?>
            <?php endif; ?>
            <?php endif; ?>

            <?php if($params->get('itemHits')): ?>
            <span class="moduleItemHits">
                <?php echo JText::_('K2_READ'); ?> <?php echo $item->hits; ?> <?php echo JText::_('K2_TIMES'); ?>
            </span>
            <?php endif; ?>

            <?php if($params->get('itemReadMore') && $item->fulltext): ?>
            <a class="moduleItemReadMore" href="<?php echo $item->link; ?>">
                <?php echo JText::_('K2_READ_MORE'); ?>
            </a>
            <?php endif; ?>

            <!-- Plugins: AfterDisplay -->
            <?php echo $item->event->AfterDisplay; ?>

            <!-- K2 Plugins: K2AfterDisplay -->
            <?php echo $item->event->K2AfterDisplay; ?>

            <div class="clr"></div>
        </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>

    <?php if($params->get('itemCustomLink')): ?>
    <a class="moduleCustomLink" href="<?php echo $itemCustomLinkURL; ?>" title="<?php echo K2HelperUtilities::cleanHtml($itemCustomLinkTitle); ?>">
        <?php echo $itemCustomLinkTitle; ?>
    </a>
    <?php endif; ?>

    <div class="clr"></div>

    <?php if($params->get('feed')): ?>
    <div class="k2FeedIcon">
        <a href="<?php echo JRoute::_('index.php?option=com_k2&view=itemlist&format=feed&moduleID='.$module->id); ?>" title="<?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?>">
            <i class="icon-feed"></i>
            <span><?php echo JText::_('K2_SUBSCRIBE_TO_THIS_RSS_FEED'); ?></span>
        </a>
        <div class="clr"></div>
    </div>
    <?php endif; ?>

</div>
